import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/*Print the level of friendship. 

 Given a person and list of his friends, print all his friends by level of association. 

 The text file will be like one below 

 A: B,C,D 
 D: B,E,F 
 E: C,F,G 

 If the input is A, the out put should be: 

 Level 1 - B,C,D 
 Level 2 - E,F 
 Level 3 - G*/

public class Friendship {
	public static void main(String[] args) {
		printLevelOfFriends(new File("Friendship.txt"), "A");
	}

	public static void printLevelOfFriends(File file, String friend) {
		try {
			Map<String, List<String>> friendMap = new HashMap<String, List<String>>();
			Scanner in = new Scanner(file);
			while (in.hasNextLine()) {
				String line = in.nextLine();
				String key = line.split(":")[0];
				List<String> values = Arrays.asList((line.split(":")[1].trim()).split(","));
				friendMap.put(key, values);
			}
			for(Map.Entry<String,List<String>> entry : friendMap.entrySet()){
				String key = entry.getKey();
				List<String> values = entry.getValue();
				for(String value : values){
					if(friendMap.containsKey(value)){
						
					}else{
						
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

}

//Given a string array ex: [1, 2, 3], find the permutation in best time.
import java.util.ArrayList;

public class Permutations {
	public static void main(String[] args) {
		permutations(new String[] { "1", "2", "3" , "4"});		

	}

	public static ArrayList<String[]> permutations(String[] a) {
		ArrayList<String[]> ret = new ArrayList<String[]>();
		permutation(a, 0, ret);
		return ret;
	}

	public static void permutation(String[] arr, int pos, ArrayList<String[]> list) {
		if (arr.length - pos == 1) {
			list.add(arr.clone());

			for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i] + " ");
			}
			System.out.println();
		} else {
			for(int i = pos; i < arr.length; i++){
	            if (i != pos) swap(arr, pos, i);
	            permutation(arr, pos+1, list);
	            if (i != pos) swap(arr, pos, i);
	        }
		}
	}

	public static void swap(String[] arr, int pos1, int pos2) {
		String h = arr[pos1];
		arr[pos1] = arr[pos2];
		arr[pos2] = h;
	}
}

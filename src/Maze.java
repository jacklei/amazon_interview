/*Problem:

Given a maze, a starting point, and an ending point, determine whether the maze has a solution.

0 0 0 0
a 1 0 0
0 1 1 0
0 0 b 0
start (1, 0) // Point 'a'
end (3, 2) // Point 'b'
result: true


0 1 0 0
a 1 0 0
0 1 1 0
0 1 b 0
start (1, 0)
end (3, 2)
result: false

 0 = is a open path
1 = is a wall*/

public class Maze {
	public static void main(String[] args) {
		String[][] one = new String[][] { 
				{ "0", "0", "0", "0" },
				{ "a", "1", "0", "0" }, 
				{ "0", "1", "1", "0" },
				{ "0", "0", "b", "0" } };
		String[][] two = new String[][] { 
				{ "0", "1", "0", "0" },
				{ "a", "1", "0", "0" }, 
				{ "0", "1", "1", "0" },
				{ "0", "1", "b", "0" } };
		System.out.println("One:");
		System.out.println("true - " + solver(one));
		printArray(one);
		System.out.println("Two:");
		System.out.println("false - " + solver(one));
		printArray(two);
	}

	public static void printArray(String[][] array) {
		String line = "________________________________________";
		System.out.println(line);
		for (int i = 0; i < array.length; i++) { // rows
			for (int j = 0; j < array[0].length; j++) {
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println(line);
	}

	public static boolean solver(String[][] array) {
		int row = 0, column = 0;
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[0].length; j++) {
				if (array[i][j] == "a") {
					row = i;
					column = j;
				}
			}
		}

		return solver(array, row, column);
	}

	public static boolean solver(String[][] array, int row, int col) {
		if (array[row][col] == "b"){
			array[row][col] = "1";
			return true;
		}
		String down, right, left, up;
		down = right = left = up = "1";
		if (row + 1 < array.length)
			down = array[row + 1][col];
		if (col + 1 < array[0].length)
			right = array[row][col + 1];
		if (col - 1 >= 0)
			left = array[row][col - 1];
		if (row - 1 >= 0)
			up = array[row - 1][col];

		boolean solved = false;
		if (array[row][col] != "b") {
			array[row][col] = "1";
		}
		if (down != "1" && !solved) {
			solved = solver(array, row + 1, col);
		}
		if (right != "1" && !solved) {
			solved = solver(array, row, col + 1);
		}
		if (left != "1" && !solved) {
			solved = solver(array, row, col - 1);
		}
		if (up != "1" && !solved) {
			solved = solver(array, row - 1, col);
		}
		return solved;
	}

}
